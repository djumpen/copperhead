<?php

error_reporting(0);

$name = $_POST['name'];
$email = $_POST['email'];
$text = $_POST['text'];

if (strlen($name) && filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($text)) {
    $message = 'Name: ' . $name . "\n";
    $message .= 'Email: ' . $email . "\n";
    $message .= 'Message: ' . $text . "\n";

    mail("kontakt@copperhead3.de", "Copperhead message", $message,
        "From: kontakt@copperhead3.de \r\n"
        ."X-Mailer: PHP/" . phpversion());

    die(json_encode(array('error' => 0, 'message' => 'Nachricht wurde gesendet')));
} else {
    die(json_encode(array('error' => 1, 'message' => 'Füllen Sie alle Felder')));
}
